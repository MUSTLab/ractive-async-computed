(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(factory());
}(this, (function () { 'use strict';

module.exports = (get, set) => {
	let _val = null;
	return {
		get: function () {
			let val = _val;
			return (typeof get === 'function') && get.call(this, (_val = null, val));
		},
		set: function (val) {
			_val = (typeof set === 'function') && set.call(this, val);
		}
	};
};

})));
