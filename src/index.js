'use strict';

module.exports = (get, set) => {
	let _val = null;
	return {
		get: function () {
			let val = _val;
			return (typeof get === 'function') && get.call(this, (_val = null, val));
		},
		set: function (val) {
			_val = (typeof set === 'function') && set.call(this, val);
		}
	};
};